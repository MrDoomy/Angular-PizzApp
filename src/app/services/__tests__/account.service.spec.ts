import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AccountService } from '../account.service';
import api from '../api';
import { environment } from '../../../environments/environment';

describe('Account: Service', () => {
  let service: AccountService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AccountService]
    });
  });

  beforeEach(() => {
    const injector = getTestBed();

    service = injector.get(AccountService);
    httpMock = injector.get(HttpTestingController);
  });
  
  afterEach(() => {
    httpMock.verify();
  });

  it("Should 'fetchLoginAccount' Returns Observable", async () => {
    service.fetchLoginAccount({
      login: 'Pickle',
      password: 'Azerty'
    }).subscribe(({ token }) => {
      expect(token).toEqual('ABCDEF123456');
    });

    const request = httpMock.expectOne(environment.baseUrl + api.login());

    request.flush({
      token: 'ABCDEF123456'
    });
  });

  it("Should 'fetchLoginAccount' Throws Error", () => {
    service.fetchLoginAccount({
      login: 'Pickle',
      password: 'Azerty'
    }).subscribe(() => {}, ({ message }) => {
      expect(message).toEqual('Login Account Failure !');
    });

    httpMock.expectOne(environment.baseUrl + api.login()).error(new ErrorEvent('Internal Server Error'), {
      status: 500
    });
  });

  it("Should 'fetchRegisterAccount' Returns Observable", async () => {
    service.fetchRegisterAccount({
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      password: 'Azerty',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70
    }).subscribe(({ token }) => {
      expect(token).toEqual('ABCDEF123456');
    });

    const request = httpMock.expectOne(environment.baseUrl + api.register());

    request.flush({
      token: 'ABCDEF123456'
    });
  });

  it("Should 'fetchRegisterAccount' Throws Error", () => {
    service.fetchRegisterAccount({
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      password: 'Azerty',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70
    }).subscribe(() => {}, ({ message }) => {
      expect(message).toEqual('Register Account Failure !');
    });

    httpMock.expectOne(environment.baseUrl + api.register()).error(new ErrorEvent('Internal Server Error'), {
      status: 500
    });
  });

  it("Should 'fetchReadAccount' Returns Observable", async () => {
    service.fetchReadAccount('ABCDEF123456').subscribe(account => {
      expect(account.login).toEqual('Pickle');
      expect(account.email).toEqual('rick.sanchez@pm.me');
      expect(account.firstName).toEqual('Rick');
      expect(account.lastName).toEqual('Sanchez');
      expect(account.gender).toEqual('M');
      expect(account.yearOld).toEqual(70);
    });

    const request = httpMock.expectOne(environment.baseUrl + api.account());

    request.flush({
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70
    });
  });

  it("Should 'fetchReadAccount' Throws Error", () => {
    service.fetchReadAccount('ABCDEF123456').subscribe(() => {}, ({ message }) => {
      expect(message).toEqual('Read Account Failure !');
    });

    httpMock.expectOne(environment.baseUrl + api.account()).error(new ErrorEvent('Internal Server Error'), {
      status: 500
    });
  });

  it("Should 'fetchLogoutAccount' Returns Observable", async () => {
    service.fetchLogoutAccount('ABCDEF123456').subscribe(({ token }) => {
      expect(token).toBeNull();
    });

    const request = httpMock.expectOne(environment.baseUrl + api.logout());

    request.flush({
      token: null
    });
  });

  it("Should 'fetchLogoutAccount' Throws Error", () => {
    service.fetchLogoutAccount('ABCDEF123456').subscribe(() => {}, ({ message }) => {
      expect(message).toEqual('Logout Account Failure !');
    });

    httpMock.expectOne(environment.baseUrl + api.logout()).error(new ErrorEvent('Internal Server Error'), {
      status: 500
    });
  });

  it("Should 'fetchPswdAccount' Returns Observable", async () => {
    service.fetchPswdAccount('ABCDEF123456', {
      oldPswd: 'Azerty',
      newPswd: 'Qwerty'
    }).subscribe(({ updatedId }) => {
      expect(updatedId).toEqual('ABCDEF123456');
    });

    const request = httpMock.expectOne(environment.baseUrl + api.pswd());

    request.flush({
      updatedId: 'ABCDEF123456'
    });
  });

  it("Should 'fetchPswdAccount' Throws Error", () => {
    service.fetchPswdAccount('ABCDEF123456', {
      oldPswd: 'Azerty',
      newPswd: 'Qwerty'
    }).subscribe(() => {}, ({ message }) => {
      expect(message).toEqual('Pswd Account Failure !');
    });

    httpMock.expectOne(environment.baseUrl + api.pswd()).error(new ErrorEvent('Internal Server Error'), {
      status: 500
    });
  });

  it("Should 'fetchUpdateAccount' Returns Observable", async () => {
    service.fetchUpdateAccount('ABCDEF123456', {
      firstName: 'Rick',
      lastName: 'Sanchez'
    }).subscribe(({ updatedId }) => {
      expect(updatedId).toEqual('ABCDEF123456');
    });

    const request = httpMock.expectOne(environment.baseUrl + api.account());

    request.flush({
      updatedId: 'ABCDEF123456'
    });
  });

  it("Should 'fetchUpdateAccount' Throws Error", () => {
    service.fetchUpdateAccount('ABCDEF123456', {
      firstName: 'Rick',
      lastName: 'Sanchez'
    }).subscribe(() => {}, ({ message }) => {
      expect(message).toEqual('Update Account Failure !');
    });

    httpMock.expectOne(environment.baseUrl + api.account()).error(new ErrorEvent('Internal Server Error'), {
      status: 500
    });
  });

  it("Should 'fetchDeleteAccount' Returns Observable", async () => {
    service.fetchDeleteAccount('ABCDEF123456').subscribe(({ deletedId }) => {
      expect(deletedId).toEqual('ABCDEF123456');
    });

    const request = httpMock.expectOne(environment.baseUrl + api.account());

    request.flush({
      deletedId: 'ABCDEF123456'
    });
  });

  it("Should 'fetchDeleteAccount' Throws Error", () => {
    service.fetchDeleteAccount('ABCDEF123456').subscribe(() => {}, ({ message }) => {
      expect(message).toEqual('Delete Account Failure !');
    });

    httpMock.expectOne(environment.baseUrl + api.account()).error(new ErrorEvent('Internal Server Error'), {
      status: 500
    });
  });
});
