const api = {
  login: () => '/api/account/login',
  register: () => '/api/account/register',
  logout: () => '/api/account/logout',
  pswd: () => '/api/account/pswd',
  account: () => '/api/account',
  pizzas: () => '/api/pizzas'
};

export default api;
