import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class NotificationService {
  message$ = new BehaviorSubject('');
  color$ = new BehaviorSubject('');

  getMessage(): Observable<string> {
    return this.message$.asObservable();
  }

  setMessage(message: string) {
    this.message$.next(message);

    setTimeout(() => {
      this.message$.next('');
    }, 3000);
  }

  getColor(): Observable<string> {
    return this.color$.asObservable();
  }

  colorizeGreen() {
    this.color$.next('green');
  }

  colorizeRed() {
    this.color$.next('red');
  }

  colorizeYellow() {
    this.color$.next('yellow');
  }
}
