import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import api from './api';
import { Pizza } from '../models/pizzas.model';
import { environment } from '../../environments/environment';

@Injectable()
export class PizzasService {

  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  /**
   * Fetch All Pizzas
   *
   * @returns {Observable} Response | Error
   */
  public fetchAllPizzas(): Observable<Pizza[]> {
    return this.http
      .get<Pizza[]>(
        environment.baseUrl + api.pizzas(),
        this.httpOptions
      )
      .pipe(catchError(() => of([])));
  }
}
