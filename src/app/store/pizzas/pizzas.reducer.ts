import { createReducer, on, Action } from '@ngrx/store';
import * as PizzasActions from './pizzas.actions';
import { PizzasReducer } from '../../models/pizzas.model';

const initialState: PizzasReducer = [];

const reducer = createReducer(
  initialState,
  on(PizzasActions.setPizzas, (_: PizzasReducer, { payload }) => {
    return payload;
  }),
  on(PizzasActions.addPizza, (state: PizzasReducer, { payload }) => {
    return [
      ...state,
      payload
    ];
  }),
  on(PizzasActions.upPizza, (state: PizzasReducer, { payload }) => {
    return state.map(pizza => pizza.id === payload.id ? payload : pizza);
  }),
  on(PizzasActions.delPizza, (state: PizzasReducer, { payload }) => {
    return state.filter(pizza => pizza.id !== payload);
  }),
  on(PizzasActions.resetPizzas, () => initialState)
);

type PizzasReducerOrUndefined = PizzasReducer | undefined;

export function pizzasReducer(state: PizzasReducerOrUndefined, action: Action) {
  return reducer(state, action);
}
