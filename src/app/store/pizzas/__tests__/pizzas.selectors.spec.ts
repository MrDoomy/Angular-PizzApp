import * as Selectors from '../pizzas.selectors';

describe('Pizzas: Selectors', () => {
  const state = {
    pizzas: [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ]
  };

  it("Should 'selectPizzas' Returns State", () => {
    expect(Selectors.selectPizzas({ pizzas: [] })).toHaveLength(0);
    expect(Selectors.selectPizzas(state)).toHaveLength(1);
  });

  it("Should 'selectPizzaById' Returns 'pizza'", () => {
    expect(Selectors.selectPizzaById(state, 'ABCDEF123456')).toEqual({
      id: 'ABCDEF123456',
      label: '4 Cheeses',
      items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    });
  });
});
