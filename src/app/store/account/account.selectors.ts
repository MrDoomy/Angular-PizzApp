import { createSelector } from '@ngrx/store';
import { AccountReducer } from '../../models/account.model';

export const selectAccount = (state: { account: AccountReducer }) => state.account;

export const selectLogin = createSelector(
  selectAccount,
  (account: AccountReducer) => account.login || ''
);

export const selectEmail = createSelector(
  selectAccount,
  (account: AccountReducer) => account.email || ''
);

export const selectFirstName = createSelector(
  selectAccount,
  (account: AccountReducer) => account.firstName || ''
);

export const selectLastName = createSelector(
  selectAccount,
  (account: AccountReducer) => account.lastName || ''
);

export const selectGender = createSelector(
  selectAccount,
  (account: AccountReducer) => account.gender || ''
);

export const selectYearOld = createSelector(
  selectAccount,
  (account: AccountReducer) => account.yearOld || ''
);

export const selectToken = createSelector(
  selectAccount,
  (account: AccountReducer) => account.token || null
);
