export interface Pizza {
  id: string;
  label: string;
  items: string[];
  price: number;
}

export type PizzasReducer = Pizza[];
