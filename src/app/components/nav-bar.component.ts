import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AccountReducer } from '../models/account.model';
import { logoutAccount, selectToken, selectFirstName } from '../store/account';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html'
})
export class NavBarComponent implements OnInit {
  @Input() elevated = true;
  @Input() spaced = true;
  
  active = false;
  isConnected: boolean;
  firstName: string;

  constructor(private store: Store<{ account: AccountReducer }>) {}

  ngOnInit() {
    this.store.pipe(select(selectToken)).subscribe(token => this.isConnected = !!token);
    this.store.pipe(select(selectFirstName)).subscribe(firstName => this.firstName = firstName);
  }

  handleClick() {
    this.store.dispatch(logoutAccount());
  }
}
