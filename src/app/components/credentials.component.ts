import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AccountReducer } from '../models/account.model';
import { pswdAccount, deleteAccount } from '../store/account';

@Component({
  selector: 'credentials',
  templateUrl: './credentials.component.html'
})
export class CredentialsComponent {
  hidden = true;

  credentialsForm = new FormGroup({
    oldPswd: new FormControl(''),
    newPswd: new FormControl(''),
    repeatPswd: new FormControl('')
  });

  constructor(private store: Store<{ account: AccountReducer }>) {}

  getError(formControlName: string, formControlNameRef?: string) {
    const formControl = this.credentialsForm.controls[formControlName];

    if (formControlNameRef) {
      const formControlRef = this.credentialsForm.controls[formControlNameRef];

      return formControl.touched && (formControl.errors || formControl.value !== formControlRef.value);
    }

    return formControl.touched && formControl.errors;
  }

  handleSubmit() {
    const { oldPswd, newPswd } = this.credentialsForm.value;

    this.store.dispatch(pswdAccount({
      oldPswd,
      newPswd
    }));
  }

  handleClick() {
    this.store.dispatch(deleteAccount());
  }
}
