import { CredentialsComponent } from './credentials.component';
import { HomeComponent } from './home.component';
import { LoginComponent } from './login.component';
import { NavBarComponent } from './nav-bar.component';
import { NotificationComponent } from './notification.component';
import { PizzaComponent } from './pizza.component';
import { ProfileComponent } from './profile.component';
import { RegisterComponent } from './register.component';
import { SettingsComponent } from './settings.component';

export {
  CredentialsComponent,
  HomeComponent,
  LoginComponent,
  NavBarComponent,
  NotificationComponent,
  PizzaComponent,
  ProfileComponent,
  RegisterComponent,
  SettingsComponent
};
