module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts'],
  testEnvironment: "jest-environment-jsdom-fourteen"
};
